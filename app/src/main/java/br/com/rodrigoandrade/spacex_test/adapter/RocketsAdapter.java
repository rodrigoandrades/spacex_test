package br.com.rodrigoandrade.spacex_test.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;
import br.com.rodrigoandrade.spacex_test.R;
import br.com.rodrigoandrade.spacex_test.modelo.Rockets;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Rodrigo on 18/03/2019.
 */

public class RocketsAdapter extends RecyclerView.Adapter {

    private List<Rockets> rocketsList;

    public RocketsAdapter(List<Rockets> rockets) {
        this.rocketsList = rockets;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View view = inflater.inflate(R.layout.item_rockets, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Rockets rockets = rocketsList.get(position);
        MyViewHolder holder = (MyViewHolder) viewHolder;

        holder.rockets_id.setText(String.valueOf(rockets.getRocket_id()));

        Picasso.get().load(rockets.getUrlPhoto()[0]).fit().into(holder.photo);
    }

    @Override
    public int getItemCount() {
        return rocketsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_rockets_id)
        TextView rockets_id;

        @BindView(R.id.item_rockets_photo)
        ImageView photo;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @OnClick(R.id.item_rockets)
        public void clickNoItem(View view) {

            Rockets selected = rocketsList.get(getAdapterPosition());

            EventBus.getDefault().post(selected);
        }
    }
}
