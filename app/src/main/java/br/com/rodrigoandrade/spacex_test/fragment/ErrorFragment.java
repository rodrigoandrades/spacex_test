package br.com.rodrigoandrade.spacex_test.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import org.greenrobot.eventbus.EventBus;
import br.com.rodrigoandrade.spacex_test.R;
import br.com.rodrigoandrade.spacex_test.events.RefreshEvent;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Rodrigo on 18/03/2019.
 */

public class ErrorFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static ErrorFragment with(Throwable error) {
        ErrorFragment errorFragment = new ErrorFragment();

        Bundle arguments = new Bundle();
        arguments.putSerializable("error", error);
        errorFragment.setArguments(arguments);

        return errorFragment;
    }


    @BindView(R.id.fragment_error_message)
    TextView message;

    @BindView(R.id.fragment_error_swipe)
    SwipeRefreshLayout swipe;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_error, container, false);
        ButterKnife.bind(this, view);

        swipe.setOnRefreshListener(this);
        swipe.setColorScheme(android.R.color.holo_blue_bright, android.R.color.holo_green_dark, android.R.color.holo_red_light);

        Bundle arguments = getArguments();

        Throwable error = (Throwable) arguments.getSerializable("error");

        message.setText(error.getMessage());

        return view;
    }

    @Override
    public void onRefresh() {
        EventBus.getDefault().post(new RefreshEvent());
    }
}
