package br.com.rodrigoandrade.spacex_test.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import java.util.ArrayList;
import br.com.rodrigoandrade.spacex_test.R;
import br.com.rodrigoandrade.spacex_test.events.RefreshEvent;
import br.com.rodrigoandrade.spacex_test.fragment.LoadingFragment;
import br.com.rodrigoandrade.spacex_test.fragment.ErrorFragment;
import br.com.rodrigoandrade.spacex_test.fragment.RocketsDetailsFragment;
import br.com.rodrigoandrade.spacex_test.fragment.RocketsListFragment;
import br.com.rodrigoandrade.spacex_test.modelo.Rockets;
import br.com.rodrigoandrade.spacex_test.services.RetrofitProvider;

/**
 * Created by Rodrigo on 18/03/2019.
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (dontHaveData(savedInstanceState)) {
            searchRockets();
            show(new LoadingFragment(), false);
        }
    }

    private void searchRockets() {
        new RetrofitProvider().searchRockets();
    }

    private boolean dontHaveData(Bundle savedInstanceState) {
        return savedInstanceState == null;
    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);

    }

    @Subscribe
    public void dealsWith(Rockets rockets) {
        show(RocketsDetailsFragment.with(rockets), true);
    }

    @Subscribe
    public void dealsWith(ArrayList<Rockets> rocketsArrayList) {

        FragmentManager manager = getSupportFragmentManager();

        Fragment fragment = manager.findFragmentById(R.id.frame_main);

        if (fragment instanceof RocketsListFragment) {

            RocketsListFragment rocketsListFragment = (RocketsListFragment) fragment;

            rocketsListFragment.add(rocketsArrayList);

        } else {
            show(RocketsListFragment.with(rocketsArrayList), false);
        }
    }

    @Subscribe
    public void dealsWith(Throwable error) {

        show(ErrorFragment.with(error), false);
    }


    @Subscribe
    public void receive(RefreshEvent event){
        searchRockets();
    }

    public void show(Fragment fragment, boolean canStackUp) {

        FragmentManager manager = getSupportFragmentManager();

        FragmentTransaction transaction = manager.beginTransaction();

        transaction.replace(R.id.frame_main, fragment);

        if (canStackUp) {
            transaction.addToBackStack(null);
        }
        transaction.commit();

    }
}
