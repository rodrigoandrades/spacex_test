package br.com.rodrigoandrade.spacex_test.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import br.com.rodrigoandrade.spacex_test.R;
import br.com.rodrigoandrade.spacex_test.adapter.RocketsAdapter;
import br.com.rodrigoandrade.spacex_test.modelo.Rockets;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Rodrigo on 18/03/2019.
 */

public class RocketsListFragment extends Fragment {
    @BindView(R.id.fragment_rockets_list)
    RecyclerView list;
    private boolean loading;
    private ArrayList<Rockets> rockets;


    public static RocketsListFragment with(ArrayList<Rockets> rockets) {
        RocketsListFragment rocketsListFragment = new RocketsListFragment();

        Bundle arguments = new Bundle();
        arguments.putSerializable("rockets", rockets);

        rocketsListFragment.setArguments(arguments);

        return rocketsListFragment;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_rockets_list, container, false);

        ButterKnife.bind(this, view);

        Bundle arguments = getArguments();
        rockets = (ArrayList<Rockets>) arguments.getSerializable("rockets");

        list.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setTitle("Rockets");
        actionBar.setSubtitle(null);

        list.setAdapter(new RocketsAdapter(rockets));
    }

    public void add(ArrayList<Rockets> another) {

        loading = false;
        rockets.addAll(another);
        list.getAdapter().notifyDataSetChanged();
    }
}
