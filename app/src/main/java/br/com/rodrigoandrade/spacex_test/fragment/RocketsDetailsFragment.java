package br.com.rodrigoandrade.spacex_test.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import br.com.rodrigoandrade.spacex_test.R;
import br.com.rodrigoandrade.spacex_test.modelo.Rockets;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Rodrigo on 18/03/2019.
 */

public class RocketsDetailsFragment extends Fragment {

    @BindView(R.id.rockets_details_photo)
    ImageView photo;

    @BindView(R.id.rockets_details_active)
    TextView active;

    @BindView(R.id.rockets_details_company)
    TextView company;

    @BindView(R.id.rockets_details_country)
    TextView country;

    @BindView(R.id.rockets_details_description)
    TextView description;

    @BindView(R.id.rockets_details_first_flight)
    TextView first_flight;

    @BindView(R.id.rockets_details_id)
    TextView id;


    public static RocketsDetailsFragment with(Rockets selected) {

        RocketsDetailsFragment rocketsDetailsFragment = new RocketsDetailsFragment();

        Bundle arguments = new Bundle();

        arguments.putSerializable("rockets", selected);

        rocketsDetailsFragment.setArguments(arguments);

        return rocketsDetailsFragment;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Rockets rockets = getRockets();

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();

        actionBar.setTitle(rockets.getRocket_id());

        actionBar.setDisplayHomeAsUpEnabled(true);

        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_rockets_details,
                container, false);

        ButterKnife.bind(this, view);

        Rockets rockets = getRockets();


        id.setText(String.valueOf(rockets.getId()));
        active.setText(rockets.getActive());
        description.setText(rockets.getDescription());
        first_flight.setText(rockets.getFirst_flight());
        country.setText(rockets.getCountry());
        company.setText(rockets.getCompany());


        Picasso.get().load(rockets.getUrlPhoto()[0]).fit().into(photo);

        return view;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
        }

        return true;
    }


    private Rockets getRockets() {
        Bundle arguments = getArguments();
        return (Rockets) arguments.get("rockets");
    }

}
