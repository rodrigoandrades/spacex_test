package br.com.rodrigoandrade.spacex_test.services;

import org.greenrobot.eventbus.EventBus;
import java.util.ArrayList;
import br.com.rodrigoandrade.spacex_test.application.SpaceXApplication;
import br.com.rodrigoandrade.spacex_test.modelo.Rockets;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rodrigo on 18/03/2019.
 */

public class RetrofitProvider {
    public static SpaceXAPI provideAPI() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(AppDefaults.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.client(httpClient.addInterceptor(logging).build()).build();

        return retrofit.create(SpaceXAPI.class);
    }

    public void searchRockets() {

        Call<ArrayList<Rockets>> call;
        call = SpaceXApplication.getAPI().searchRockets();

        call.enqueue(new Callback<ArrayList<Rockets>>() {
            @Override
            public void onResponse(Call<ArrayList<Rockets>> call, Response<ArrayList<Rockets>> response) {
                ArrayList<Rockets> rocketsArrayList = response.body();
                EventBus.getDefault().post(rocketsArrayList);
            }

            @Override
            public void onFailure(Call<ArrayList<Rockets>> call, Throwable erro) {
                EventBus.getDefault().post(erro);

            }
        });
    }

}