package br.com.rodrigoandrade.spacex_test.modelo;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Created by Rodrigo on 18/03/2019.
 */

public class Rockets implements Serializable {
    @SerializedName("id") private long id;
    @SerializedName("description") private String description;
    @SerializedName("first_flight") private String first_flight;
    @SerializedName("country") private String country;
    @SerializedName("company") private String company;
    @SerializedName("rocket_id") private String rocket_id;
    @SerializedName("active") private String active;
    @SerializedName("flickr_images") private String[] urlPhoto;

    public Rockets(){

    }

    public Rockets(String first_flight, String country, String company) {
        this.first_flight = first_flight;
        this.country = country;
        this.company = company;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFirst_flight() {
        return first_flight;
    }

    public void setFirst_flight(String first_flight) {
        this.first_flight = first_flight;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getRocket_id() {
        return rocket_id;
    }

    public void setRocket_id(String rocket_id) {
        this.rocket_id = rocket_id;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String[] getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String[] urlPhoto) {
        this.urlPhoto = urlPhoto;
    }
}
