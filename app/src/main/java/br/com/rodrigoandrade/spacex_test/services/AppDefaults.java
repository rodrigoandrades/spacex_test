package br.com.rodrigoandrade.spacex_test.services;

/**
 * Created by Rodrigo on 18/03/2019.
 */

public class AppDefaults {
    public static final String BASE_URL = "https://api.spacexdata.com/v3/";
}
