package br.com.rodrigoandrade.spacex_test.application;

import android.app.Application;
import com.jakewharton.threetenabp.AndroidThreeTen;
import br.com.rodrigoandrade.spacex_test.services.RetrofitProvider;
import br.com.rodrigoandrade.spacex_test.services.SpaceXAPI;

/**
 * Created by Rodrigo on 18/03/2019.
 */

public class SpaceXApplication extends Application {
    private static final SpaceXAPI API = RetrofitProvider.provideAPI();

    public static SpaceXAPI getAPI() {
        return API;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
    }
}
