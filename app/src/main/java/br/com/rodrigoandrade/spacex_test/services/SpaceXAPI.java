package br.com.rodrigoandrade.spacex_test.services;

import java.util.ArrayList;
import br.com.rodrigoandrade.spacex_test.modelo.Rockets;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Rodrigo on 18/03/2019.
 */

public interface SpaceXAPI {
    @GET("rockets")
    Call<ArrayList<Rockets>> searchRockets();

}

