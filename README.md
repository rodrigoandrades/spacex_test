# spacex_test

Utilizei bibliotecas muito usadas em projetos Android e conhecidas na comunidade, tais como:
- RecyclerView para reciclagem das listas;
- Butterknife para injeção de View;
- Picasso para carregar imagens;
- Retrofit para request dos foguetes e para conveter o Json;
- EventBus para disparar eventos;
- Logging-interceptor para ver os jsons no LogCat;